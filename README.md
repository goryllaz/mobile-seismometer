# Mobilní Seismograf SMAP projekt

Projekt vytvořený studentem fakulty informatiky a managementu Univerzity Hradec Králové pro předmět Smart přístupy k tvorbě IS a aplikací

## Cíl projektu

Cílem projektu je vytvoření mobilní a serverové aplikace pro měření seismické aktivity pomocí více mobilů a jejich akcelerometrického senzoru. V současné době je akcelometr v mobilních zařízeních dostatečně citlivý na to, aby zaznamenal zemětřesení v klidovém režimu. Problémem je, že ne vždy může být mobil v klidném režimu. Proto by autor tohoto projektu chtěl udělat jednak aplikaci která na mobilu zaznamená aktivitu, ale také ji zasílá na server a pomocí GPS souřadnic porovnává aktivitu s ostatními mobily v okolí. Pokud se potom omylem pohne s jedním z mobilů v okolí, výsledné hodnoty seismografu by to nemělo ovlivnit. Kvalita měření pak bude závislá na počtu zařízení, které sledují v okolí aktivitu. Díky tomu se, ale i zvýší kvalita měření. Výsledky pak pro jednotlivé oblasti budou k dispozici ve webové aplikaci na mapě.

## Mobilní aplikace

Aplikace pro mobilní zařízení by měla být cross-platformní. Po spuštění a povolení sdílení polohových informací se uživateli aplikace zobrazí seismograf. V tento moment už aplikace posílá informace na server. Uživateli se ale nebude ukazovat seismická aktivita jeho mobilního zařízení, ale už informace, která pochází díky více zařízení v okolí. V zařízení také musí dojít k synchronizaci mezi serverem a zařízením. Důvodem je, aby všechna zařízení měřila co nejvíce ve stejný čas, aby server nedostával stará data.

## Serverová aplikace

Serverová aplikace bude nejspíše psána jazyce Python z důvodu frameworku ObsPy, který bude zpracovávat naměřená data. To znamená, že je potřeba vytvořit rozhraní na které bude mobilní aplikace zasílat informace a také informace získávat.  Serverová aplikace bude mít také svoje webové grafické rozhraní, kde bude vidět mapa se seismickou aktivitou.